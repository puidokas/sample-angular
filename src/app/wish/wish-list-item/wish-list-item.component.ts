import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { WishItem } from 'src/shared/models/wishItem';
// Hard dependency
// import events from './../../shared/services/EventService';
// Dependency injection
import {EventService} from '../../../shared/services/EventService';

@Component({
  selector: 'wish-list-item',
  templateUrl: './wish-list-item.component.html',
  styleUrls: ['./wish-list-item.component.css']
})
export class WishListItemComponent {
  // Non-null assertion operator
  @Input() wish! : WishItem;

  // @Input() fullfilled! : boolean;
  // @Output()fullfilledChange = new EventEmitter<boolean>();

  get cssClasses() {
    // Or: 'strikeout text-muted'
    // Or: return this.fullfilled ? ['strikeout', 'text-muted'] : [];
    // Or: return {'strikeout': this.fullfilled, 'text-muted': this.fullfilled}
    return {'strikeout text-muted': this.wish.isComplete};
  }

  // "private" creates property automatically
  constructor(private events: EventService) {

  }

  removeWish() {
    this.events.emit('removeWish', this.wish);
  }

  toggleFullfilled() {
    this.wish.isComplete = !this.wish.isComplete;
  }

}
