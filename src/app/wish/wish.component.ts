import { Component } from '@angular/core';
import { WishItem } from 'src/shared/models/wishItem';
import { EventService } from './../../shared/services/EventService';
import { WishService } from '../wish/wish.service';

@Component({
  selector: 'app-wish',
  templateUrl: './wish.component.html',
  styleUrls: ['./wish.component.css']
})
export class WishComponent {
  items : WishItem[] = [];
  // items : WishItem[] = [
  //   new WishItem('To learn Angular'),
  //   new WishItem('Get Coffee', true),
  //   new WishItem('Find grass that cuts itself')
  // ]

// Do not need "private", because we are using the events object directly in the constructor
constructor(events: EventService, private wishService: WishService) {
  events.listen('removeWish', (wish : any) => {
    let index = this.items.indexOf(wish);
    this.items.splice(index, 1);
  });
}

ngOnInit() : void {
  this.wishService.getWishes().subscribe(
    (data : any) => {
      this.items = data;
    },
    (error: any) => {
      alert(error.message);
    }
  );
}

  // filter : any = () => {};
  filter : any;
}
