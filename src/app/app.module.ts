import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { WishModule } from './wish/wish.module';
import { ContactModule } from './contact/contact.module';
import { FirstComponent } from './first/first.component';
import { SecondComponent } from './second/second.component';

import { AppRoutingModule } from './app-routing.module';
import { NotFoundComponent } from './not-found/not-found.component';
import { ProductsModule } from './products/products.module';

@NgModule({
  // Set of components or directives that belong to this particular module
  declarations: [
    AppComponent,
    FirstComponent,
    SecondComponent,
    NotFoundComponent
  ],
  // Things available to the templates inside this module
  imports: [
    BrowserModule,
    WishModule,
    ContactModule,
    AppRoutingModule,
    ProductsModule
  ],
  // Objects that can be injected to parts of this module
  providers: [],
  // providers: [EventService],
  // Entry point of the module
  bootstrap: [AppComponent]
})
export class AppModule { }
