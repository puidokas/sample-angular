import { Injectable } from "@angular/core";
import { Observable, Subject} from "rxjs";

// Also needs to be put in app.module.ts file as a provider
// @Injectable()
@Injectable({
    // providedIn: AppModule
    providedIn: 'root'
    // providedIn: 'platform'
    // providedIn: 'any'
})
export class EventService {
    private subject = new Subject();

    emit(eventName : string, payload : any) {
        this.subject.next({eventName, payload});
    }

    listen(eventName: string, callback : (event: any) => void) {
        this.subject.asObservable().subscribe((nextObj : any) => {
            if (eventName === nextObj.eventName) {
                callback(nextObj.payload);
            }
        });
    }
}